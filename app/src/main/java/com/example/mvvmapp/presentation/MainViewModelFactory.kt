package com.example.mvvmapp.presentation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmapp.data.respository.UserRepositoryImpl
import com.example.mvvmapp.data.storage.SharedPrefUserStorage
import com.example.mvvmapp.domain.usecase.GetUserNameUseCase
import com.example.mvvmapp.domain.usecase.SaveUserNameUseCase

class MainViewModelFactory(context: Context): ViewModelProvider.Factory {


    private val userRepository by lazy(LazyThreadSafetyMode.NONE) {  UserRepositoryImpl(
        SharedPrefUserStorage(context = context)
    ) } // Lazy by инициализация только тогда когда нужен метод
    private val getUserNameUseCase by lazy(LazyThreadSafetyMode.NONE) { GetUserNameUseCase(userRepository) }
    private val saveUserNameUseCase by lazy(LazyThreadSafetyMode.NONE) { SaveUserNameUseCase(userRepository) }
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(
            getUserNameUseCase = getUserNameUseCase,
            saveUserNameUseCase = saveUserNameUseCase
        ) as T
    }
}