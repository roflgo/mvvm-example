package com.example.mvvmapp.data.respository

import com.example.mvvmapp.data.storage.User
import com.example.mvvmapp.data.storage.UserStorage
import com.example.mvvmapp.domain.models.SaveUserNameParam
import com.example.mvvmapp.domain.models.UserName
import com.example.mvvmapp.domain.repository.UserRepository

class UserRepositoryImpl(private val userStorage: UserStorage): UserRepository {// Связующее звяно для связи всего вместе, можно так же прописать private val networkApi: NetworkApi для метода отправки в интернет
    // Не должно быть скрытой логики желательно обходится без if,else без условий и подобного


    override fun saveName(saveparam: SaveUserNameParam): Boolean { // Метод переписанный сохранения данных
        val user = User(firstName = saveparam.name, lastName = "")

        val result = userStorage.save(user) // Сохраняем данные локально

        return result
    }

    override fun getName(): UserName { // Метод переписанный получения данных
        val user = userStorage.get() // Получаем сохраненные данные

        val userName = UserName(firstName = user.firstName, lastName = user.lastName) // Получаем данные

        return userName
    }
}