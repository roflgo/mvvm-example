package com.example.mvvmapp.data.storage

import android.content.Context

private const val SHARED_NAME = "main"
private const val KEY_FIRST_NAME = "firstName"
private const val KEY_LAST_NAME = "lastName"

class SharedPrefUserStorage(context: Context): UserStorage { // Класс занимающийся сугубо сохранением данных в SharedPreferences
    // Не должно быть скрытой логики желательно обходится без if,else без условий и подобного
    private val sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE) // Инициализация SharedPreferences
    override fun save(user: User): Boolean {
        sharedPreferences.edit().putString(KEY_FIRST_NAME, user.firstName).apply() // записываем данные в SharedPreferences
        sharedPreferences.edit().putString(KEY_LAST_NAME, user.lastName).apply() // записываем данные в SharedPreferences
        return true
    }

    override fun get(): User {
        val firstName = sharedPreferences.getString(KEY_FIRST_NAME, "") ?: "" // Получаем firstName по дефолту будет в случае null ""
        val lastName = sharedPreferences.getString(KEY_LAST_NAME, "Default last name") ?:"Default last name"  // Получаем lastName по дефолту будет в случае null "Default last name"
        return User(firstName, lastName)
    }
}