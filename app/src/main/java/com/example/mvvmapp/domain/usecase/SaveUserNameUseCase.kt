package com.example.mvvmapp.domain.usecase

import com.example.mvvmapp.domain.models.SaveUserNameParam
import com.example.mvvmapp.domain.repository.UserRepository

class SaveUserNameUseCase(private val userRepository: UserRepository) {
    // Все условия должны быть прописаны в useCase
    fun execute(param: SaveUserNameParam): Boolean {

        val oldUserName = userRepository.getName()
        if (oldUserName.firstName == param.name) { // В случае если старое имя пользователя уже есть то выводим true
            return true
        }

        val result: Boolean = userRepository.saveName(param)
        return result
    }
}